const router = require("express").Router();
const bodyParser = require("body-parser");

const urlEncoded = bodyParser.urlencoded({ extended: false });

const auth = require("./controllers/authController");
const restrict = require("./middlewares/restrict");

router.get("/", restrict, (req, res) => res.render("index"));

router.get("/register", (req, res) => res.render("register"));
router.post("/register", urlEncoded, auth.register);

router.get("/login", (req, res) => res.render("login"));
router.post("/login", urlEncoded, auth.login);

router.get("/whoami", restrict, auth.whoami);

module.exports = router;