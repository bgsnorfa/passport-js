const express = require("express");
const session = require("express-session");
const flash = require("express-flash");
const bodyParser = require("body-parser");

const app = express();
const { PORT = 3000 } = process.env;

const urlEncoded = bodyParser.urlencoded({ extended: false });

app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false
}));

const passport = require("./lib/passport");
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

app.set('view engine', 'ejs');

const router = require("./router");

app.use(router);

app.listen(PORT, () => {
    console.log(`Server is running at http://localhost:${PORT}`);
})
